# README #

A simple web service that authenticates to Google, collects data from several calendars, and combines into a singe feed for the Fourwins digital sign.

### Demonstration of google-api-php-client in a simple app ###

* Service Authentication
* Google Calendar API Usage
* [More Info](http://web-services.lsait.lsa.umich.edu/present/um-google-apps)

### How do I get set up? Edit index.php ###

* Add your client ID, email and path to p12 key 
* Dependencies google-api-php-client, PHP 5.2.1 or higher, PHP JSON extension

